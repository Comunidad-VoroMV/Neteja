
 
<img src="img/logo_readme.png" style="margin-top: 2rem">

<p>Este es el Script de Voro MV, para limpiar el sistema del pingüino, es válido para los gestores de paquetes <STRONG>APT, DNF, PACMAN y ZYPPER</STRONG>.</p>
<p>Se llama <STRONG>Neteja</STRONG> y está en su 6ª versión (1ª versión en Python). Con él, vamos a actualizar el sistema , Flatpak y/o Snap, si los tienes instalados. Finalmente eliminaremos toda la basurilla de nuestro sistema operativo.</p>
 
### Notas:
<ul>
<li>En el proceso, tendrás que poner tu contraseña de usuario.</li>
<li>A partir de la versión 6.0 de Neteja se requiere <strong>Python 3.0</strong> o superior para ejecutarse (La mayoría de las distribuciones lo trae preinstalado o lo instala como dependencia de algún programa).</li>
</ul>
 
 
## Funcionamiento del Script
 
<p>Para utilizar Neteja lo único que tendrás que hacer es descargarlo o clonar el repositorio en un directorio de tu sistema y ejecutarlo. </p>
<p>Puedes hacerlo mediante la terminal con el siguiente comando:</p>

```bash
git clone https://gitlab.com/Comunidad-VoroMV/Neteja.git  #Clonará el repositorio, creando la carpeta Neteja
```

<p>
Al clonar el programa, se descargará una carpeta llamada Neteja. </p>

<p>Puedes entrar en ella y ejecutar el programa mediante la terminal con el siguiente comando:</p>

```bash
python3 neteja.py  #Ejecutará Neteja
```
<p>
Neteja identificará qué gestor de paquetes tiene tu sistema y empezará a actualizarlo y a limpiarlo después de que le indiques la contraseña del usuario root.
</p>

 
<p>El programa tiene la opción de instalarse en el sistema si no lo está, con lo que si eliges hacerlo no tendrás que preocuparte más de su ejecución, lo hallarás en tu sistema como cualquier otra aplicación.</p>
<p>También detectará si existen actualizaciones del programa y te invitará a hacerlo cuando las haya. Si detecta que está instalado, te dará la opción de desistalarlo si deseas.</p>
 
