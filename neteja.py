#!/usr/bin/env python

from src.main import *


# Variables comunes y de configuración
titulo="https://gitlab.com/Comunidad-VoroMV/Neteja"
salida="$HOME/neteja.log"
#export NO_AT_BRIDGE=1

# Vector de posibles gestores de paquetes
arrayGestores=["dpkg","apt","dnf","pacman","zypper"]


# Variable que guardará la posición del vector de paquetes al correcto
#    Si sn = 5, entonces no está soportado
sn = 0

# Recorrido del gestor de paquetes y averiguación de cuál es el correcto
for gestor in arrayGestores:
    if gestor=="dpkg":
        path = "/usr/bin/_neon.calamares"
        if os.path.isfile(path):
            break
    else:
        sn = sn + 1
        path = "/usr/bin/"+gestor
        if os.path.isfile(path):
            break


# Comprueba si Neteja está soportado, si es así ejecuta el programa principal
if not(os.path.isfile(path)):
    sn = 5
    print("NETEJA NO SOPORTADO")
else:
    gestor = str(arrayGestores[sn]).upper()
    bienvendia(gestor,sn)
