"""
    MÓDULOS DE EJECUCIÓN DE ACCIONES
"""

import gi
import os
import time
import threading

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GLib, GObject

from src.aux import *

# Actualización del sistema por el gestor detectado, Flatpak y Snap
#  Parámetros:
#       password: contraseña introducida de superusuario
#       gestor: nombre del gestor detectado
#       sn: código del gestor detectado, posición en el array
def actualiza(password,gestor,sn):
    salida(" ")
    salida('*******  ACTUALIZANDO EL SISTEMA CON ' + gestor + '  *******')
    salida(" ")

    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Actualizando el sistema con " + gestor)
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():
            #Array de gestores ["dpkg","apt","dnf","pacman","zypper"]
            if sn == 0:
                GLib.idle_add(update_progress, "pkcon refresh")
                comando(password,"pkcon refresh")
                GLib.idle_add(update_progress, "pkcon update")
                comando(password,"pkcon update")

            if sn == 1:
                GLib.idle_add(update_progress, "apt update -y")
                comando(password,"apt update -y")
                GLib.idle_add(update_progress, "apt upgrade -y")
                comando(password,"apt upgrade -y")

            if sn == 2:
                GLib.idle_add(update_progress, "dnf upgrade --refresh -y")
                comando(password,"dnf upgrade --refresh -y")

            if sn == 3:
                GLib.idle_add(update_progress, "pacman -Syyu --noconfirm")
                comando(password,"pacman -Syyu --noconfirm")

            if sn == 4:
                GLib.idle_add(update_progress, "zypper refresh")
                comando(password,"zypper refresh")
                GLib.idle_add(update_progress, "zypper update -y")
                comando(password,"zypper update -y")

            GLib.idle_add(update_progress, "flatpak update -y")
            salida(" ")
            salida('*******  ACTUALIZANDO FLATPAK  *******')
            salida(" ")
            if os.path.isfile("/usr/bin/flatpak"):
                comando(password,"flatpak update -y")
            else:
                salida("Flatpak no detectado en el sistema")


            GLib.idle_add(update_progress, "snap refresh")
            salida(" ")
            salida('*******  ACTUALIZANDO SNAP  *******')
            salida(" ")
            if os.path.isfile("/usr/bin/snap"):
                comando(password,"snap refresh")
            else:
                salida("Snap no detectado en el sistema")

            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()



# Borrado de temporales y de paquetes huérfanos e innecesarios
#  Parámetros: 
#       password: contraseña introducida de superusuario
#       gestor: nombre del gestor detectado
#       sn: código del gestor detectado, posición en el array
def borra_temporales(password,gestor,sn):
    salida(" ")
    salida('*******  BORRANDO TEMPORALES  *******')
    salida(" ")    

    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Borrando temporales")
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():
            #Array de gestores ["dpkg","apt","dnf","pacman","zypper"]
            if sn == 0 or sn == 1:
                GLib.idle_add(update_progress, "apt purge")
                salida(" ")
                salida('-Purgandos los paquetes innecesarios')
                salida(" ")
                comando(password,"apt purge")

                GLib.idle_add(update_progress, "apt clean -y")
                salida(" ")
                salida('-Borrando caché de APT')
                salida(" ")
                comando(password,"apt clean -y")

                GLib.idle_add(update_progress, "apt autoclean -y")
                salida(" ")
                salida('-Borrando versiones antiguas de paquetes')
                salida(" ")
                comando(password,"apt autoclean -y")
                GLib.idle_add(update_progress, "apt autoremove -y")

                salida(" ")
                salida('-Borrando paquetes huérfanos')
                salida(" ")
                comando(password,"apt autoremove -y")

            if sn == 2:
                GLib.idle_add(update_progress, "dnf clean all -y")
                salida(" ")
                salida('-Borrando paquetes antiguos')
                salida(" ")
                comando(password,"dnf clean all -y")

                GLib.idle_add(update_progress, "dnf autoremove -y")
                salida(" ")
                salida('-Borrando paquetes huérfanos')
                salida(" ")
                comando(password,"dnf autoremove -y")
                GLib.idle_add(update_progress, 'rm -vfr /tmp/*')

            if sn == 3:
                GLib.idle_add(update_progress, "pacman -Scc --noconfirm")
                salida(" ")
                salida('-Borrando caché de pacman')
                salida(" ")
                comando(password,"pacman -Scc --noconfirm")

                GLib.idle_add(update_progress, "pacman -Rns $(pacman -Qtdq) --noconfirm")
                salida(" ")
                salida('-Borrando paquetes huérfanos')
                salida(" ")
                comando(password,"pacman -Rns $(pacman -Qtdq) --noconfirm")

            if sn == 4:
                GLib.idle_add(update_progress, "zypper clean")
                salida(" ")
                salida('-Borrando caché y paquetes no usados')
                salida(" ")
                comando(password,"zypper clean")
                GLib.idle_add(update_progress, 'rm -vfr /tmp/*')

            GLib.idle_add(update_progress, 'find /tmp -mtime +1 -delete')
            salida(" ")
            salida('-Borrando archivos temporales de /tmp con más de 1 día de antigüedad')
            salida(" ")
            time.sleep(1)
            comando(password,"find /tmp -mtime +1 -delete")

            GLib.idle_add(update_progress, "find /var/tmp -mtime +1 -delete")
            salida(" ")
            salida('-Borrando archivos temporales de /var/tmp con más de 1 día de antigüedad')
            salida(" ")
            time.sleep(1)
            comando(password,"find /var/tmp -mtime +1 -delete")

            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()


# Actualización del sistema por el gestor detectado
#  Parámetros: 
#       password: contraseña introducida de superusuario
#       gestor: nombre del gestor detectado
#       sn: código del gestor detectado, posición en el array
def borra_papelera(password):
    salida(" ")
    salida('*******  BORRANDO PAPELERA  *******')
    salida(" ")    

    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Borrando papelera")
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():

            GLib.idle_add(update_progress, 'rm -rf ~/.local/share/Trash/*')
            time.sleep(1)
            comando(password,'rm -rf ~/.local/share/Trash/*')

            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()
