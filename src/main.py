"""
    INTERFACE GRÁFICA DEL PROGRAMA
    - VENTANA PRINCIPAL DE DATOS -
"""

import gi
import os
import time

from src.aux import *
from src.exe import *
from src.programa import *


# Pantalla inicial de bienvenida donde muestra el gestor de paquetes detectado
#  Si se da a cancelar se cierra la app, si se da aceptar entonces pide contraseña
#  Parámetros: 
#       gestor: gestor de paquetes en texto
#       sn: código del gestor en el array
def bienvendia(gestor,sn):

    def aceptar_boton(button):
        windows.destroy()
        # Llamada al ḿódulo de petición de contraseña
        super_usuario(gestor,sn)

    builder = Gtk.Builder()
    builder.add_from_file("neteja.glade")
    handlers = {
        "cerrar_app": Gtk.main_quit,
        "aceptar_click": aceptar_boton,
    }
    builder.connect_signals(handlers)
    windows = builder.get_object("bienvenida")

    etiqueta_gestor = builder.get_object("etiqueta_gestor")
    etiqueta_gestor.set_text(gestor)

    windows.show_all()

    Gtk.main()

# Pantalla de petición de contraseña
#  Permite hasta 3 errores, si se falla más sale de la app cargando la pantalla de error
#  Si se introduce bien, carga el módulo de pantalla principal
#  Parámetros: 
#       gestor: gestor de paquetes en texto
#       sn: código del gestor en el array
def super_usuario(gestor,sn):
    def cargar_ventana(errores):
        def aceptar_boton(button):
            password = password_box.get_text()
            error = comprobar_pass(password)
            if error == 0:
                windows.destroy()
                # Parece que todo va bien, inicia el fichero LOG donde se guardarán las salidas
                ini_salida()
                # Carga la pantalla princial
                ventana_ppal(password,gestor,sn)

            else:
                errores = n_error + 1
                if errores == 3:
                    windows.destroy()
                    ventana_error_pass()
                else:
                    windows.destroy()
                    # Se ha introducido demasiadas veces la contraseña mal y se carga la pantalla de errores
                    cargar_ventana(errores)

        n_error = errores

        if n_error == 0:
            msg_error = ""
        else:
            msg_error = "Contraseña incorrecta (intento " + str(n_error+1) + "). Inténtelo de nuevo"

        builder = Gtk.Builder()
        builder.add_from_file("neteja.glade")
        handlers = {
            "cerrar_app": Gtk.main_quit,
            "aceptar_click": aceptar_boton,
        }

        builder.connect_signals(handlers)
        windows = builder.get_object("super_usuario")
        etiqueta_error = builder.get_object("etiqueta_error")
        etiqueta_error.set_text(msg_error)
        password_box =  builder.get_object("password")
        password_box.set_text("")


        windows.show_all()

        Gtk.main()

    cargar_ventana(0)

# Pantalla de error en la contraseña. Se saldrá de la aplicación
def ventana_error_pass():
    builder = Gtk.Builder()
    builder.add_from_file("neteja.glade")
    handlers = {
        "cerrar_app": Gtk.main_quit,
    }
    builder.connect_signals(handlers)
    windows = builder.get_object("error_pass")

    windows.show_all()

    Gtk.main()


# Ventana principal de acciones a realizar: actualizar, borrar temporales ..., borrar papelera
#  Parámetros: 
#       password: contraseña de superusuario
#       gestor: gestor de paquetes en texto
#       sn: código del gestor en el array
def ventana_ppal(password,gestor,sn):

    home = os.environ['HOME']
    path_install = home + "/.ComunidadVoroMV"

    esta_neteja = os.path.isfile(path_install + "/Neteja/neteja.py")
    hay_actualizaciones = False


    def aceptar_boton(button):
        instala = instala_check.get_active()
        actualizar = actualiza_check.get_active()
        temporales = temporales_check.get_active()
        papelera = papelera_check.get_active()
        windows.destroy()
        if actualizar:
            # Si se decide actualizar llama al módulo de actualizar
            actualiza(password,gestor,sn)
        if temporales:
            # Si se decide borrar temporales,... llama al módulo de borrar temporales
            borra_temporales(password,gestor,sn)
        if papelera:
            # Si se decide borrar papelera llama al módulo de borrar papelera
            borra_papelera(password)
        if instala:
            if esta_neteja:
                if hay_actualizaciones:
                    salida(" ")
                    salida("SE ACTUALIZARÁ NETEJA CUANDO SE CIERRE ESTA APLIACIÓN")
                    despedida()
                    neteja_update(password,path_install)
                else:
                    salida(" ")
                    salida("SE DESINSTALARÁ NETEJA CUANDO SE CIERRE ESTA APLIACIÓN")
                    despedida()
                    desinstalar_neteja(password,path_install)
            else:
                salida(" ")
                salida("SE INSTALARÁ NETEJA CUANDO SE CIERRE ESTA APLIACIÓN")
                despedida()
                instalar_neteja(password,path_install)
        else:
            despedida()
    
    builder = Gtk.Builder()
    builder.add_from_file("neteja.glade")
    handlers = {
        "cerrar_app": Gtk.main_quit,
        "aceptar_click": aceptar_boton,
    }
    builder.connect_signals(handlers)
    windows = builder.get_object("ventana_ppal")

    instala_check =  builder.get_object("instala_check")
    actualiza_check = builder.get_object("actualiza_check")
    temporales_check = builder.get_object("temporales_check")
    papelera_check = builder.get_object("papelera_check")

    text_instala = builder.get_object("label_instala")

    if esta_neteja:
        hay_actualizaciones=actualizar_neteja(password,path_install)
        if hay_actualizaciones:
            text_instala.set_text("Actualizar aplicación de Neteja (se hará al finalizar)")
            instala_check.set_active(True)
        else:
            text_instala.set_text("Desinstalar aplicación de Neteja (se hará al finalizar)")
            instala_check.set_active(False)
    else:
        instala_check.set_active(True)
        text_instala.set_text("Instalar Neteja en el sistema (se hará al finalizar)")

    windows.show_all()
    Gtk.main()

# Ventana final de despedida. Muestra la opción de mostrar el fichero LOG
def despedida():
    def aceptar_boton(button):
        # Si se quiere ver LOG llama al módulo de ver LOG
        ver_log()
    
    builder = Gtk.Builder()
    builder.add_from_file("neteja.glade")
    handlers = {
        "cerrar_app": Gtk.main_quit,
        "aceptar_click": aceptar_boton,
    }
    builder.connect_signals(handlers)
    windows = builder.get_object("despedida")

    windows.show_all()

    Gtk.main()

# Ventana que muestra el fichero LOG. Sólo se puede cerrar esta ventana
def ver_log():
    def cerrar_app(button):
        windows.destroy()
        Gtk.main_quit()
    def aceptar_click(button):
        windows.destroy()
        Gtk.main_quit()
    builder = Gtk.Builder()
    builder.add_from_file("neteja.glade")
    handlers = {
        "cerrar_app": cerrar_app,
        "aceptar_click": aceptar_click,
    }
    builder.connect_signals(handlers)
    windows = builder.get_object("log_file")

    text_log = builder.get_object("text_log")
    home = os.environ['HOME']
    file = open(home+'/neteja.log','r')
    text=file.read()
    text_log.set_text(text)

    windows.show_all()

    Gtk.main()
    
