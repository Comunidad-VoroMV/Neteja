"""
    MÓDULOS PARA INSTALAR/ACTUALIZAR/DESINSTALAR NETEJA
    Interacción con Gitlab
"""

import gi
import os
import threading

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk, GLib, GObject

from src.aux import *

# Parámetro branch para clonar y actualizar git. Halla branch del directorio de ejecución
fich_branch = ".neteja.branch"
comando_sinsudo("rm " + fich_branch)
comando_sinsudo("git branch >>" + fich_branch + " 2>&1")

b = open(fich_branch, "r+")
branch = b.readline().rstrip().replace('* ','')
b.close()

if ("fatal" in branch):
    branch = "main"

# Obtenemos la ruta de la home 
home = os.environ['HOME']

def crear_desktop(path_install):
    with open(home + "/.local/share/applications/neteja.desktop","w+") as writer:
        writer.write("[Desktop Entry]" + "\n")
        writer.write("Categories=System" + "\n")
        writer.write("Comment[es_ES]=Este es el Script de Voro MV, para limpiar el sistema del pingüino, es válido para los gestores de paquetes APT, DNF, PACMAN y ZYPPER" + "\n")
        writer.write("Comment=Este es el Script de Voro MV, para limpiar el sistema del pingüino, es válido para los gestores de paquetes APT, DNF, PACMAN y ZYPPER" + "\n")
        writer.write("Exec=python3 " + path_install + "/Neteja/neteja.py" + "\n")
        writer.write("GenericName[en_US]=Limpiador del sistema del pingüino" + "\n")
        writer.write("GenericName=Limpiador del sistema del pingüino" + "\n")
        writer.write("Icon=" + path_install + "/Neteja/img/logo.png" + "\n")
        writer.write("MimeType=" + "\n")
        writer.write("Name[en_US]=Neteja" + "\n")
        writer.write("Name=Neteja" + "\n")
        writer.write("Path=" + path_install + "/Neteja" + "\n")
        writer.write("StartupNotify=true" + "\n")
        writer.write("Terminal=false" + "\n")
        writer.write("TerminalOptions=" + "\n")
        writer.write("Type=Application" + "\n")
        writer.write("Version=6.0" + "\n")
        writer.write("X-DBUS-ServiceName=" + "\n")
        writer.write("X-DBUS-StartupType=" + "\n")
        writer.write("X-KDE-SubstituteUID=false" + "\n")
        writer.write("X-KDE-Username=" + "\n")
    writer.close()


# Instalación de Neteja en path_install y copia del .desktop en $HOME/.local/share/applications
#  Parámetros: 
#       password: contraseña introducida de superusuario
#       path_install: ruta de la instalación del script
def instalar_neteja(password,path_install):
    # Imprimimos en el fichero LOG los comandos ejecutados
    salida(" ")
    salida("*******  INSTALANDO NETEJA  *******")
    salida(" ")
    salida("cd " + path_install + "/Neteja")
    salida("git clone https://gitlab.com/Comunidad-VoroMV/Neteja.git")
    salida("Creando neteja.desktop en " + home + "/.local/share/applications")

    comando_sinsudo("mkdir " + path_install)
    comando_sinsudo("mkdir " + home + "/.local/share/applications")

    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Instalando aplicación")
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():
            GLib.idle_add(update_progress, "git clone https://gitlab.com/Comunidad-VoroMV/Neteja.git")
            time.sleep(1)
            # Clona git en en /usr/bin/
            comando_sinsudo("cd " + path_install + " && git clone https://gitlab.com/Comunidad-VoroMV/Neteja.git -b " + branch)

            GLib.idle_add(update_progress, "Creando neteja.desktop")
            time.sleep(1)
            # Crear .desktop
            crear_desktop(path_install)

            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()

# Desinstalación de Neteja en path_install
#  Parámetros: 
#       password: contraseña introducida de superusuario
#       path_install: ruta de la instalación del script
def desinstalar_neteja(password,path_install):
    # Imprimimos en el fichero LOG los comandos ejecutados
    salida(" ")
    salida("*******  DESINSTALANDO NETEJA  *******")
    salida(" ")
    salida("sudo rm " + path_install + "/Neteja -R")
    salida("rm  " + home + "/.local/share/applications/neteja.desktop")

    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Desinstalando aplicación")
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():
            GLib.idle_add(update_progress, "rm " + path_install + "/Neteja -R")
            time.sleep(1)
            comando(password,"rm " + path_install + "/Neteja -R")

            GLib.idle_add(update_progress, "rm " + home + "/.local/share/applications/neteja.desktop")
            time.sleep(1)
            comando_sinsudo("rm  " + home + "/.local/share/applications/neteja.desktop")

            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()

# Comprueba si existen actualizacíones de Neteja en Gitlab
#   Si detecta actualizaciones pregunta si se desean hacer
#  Parámetros: 
#       password: contraseña introducida de superusuario
#       path_install: ruta de la instalación del script
def actualizar_neteja(password,path_install):
    # Imprimimos en el fichero LOG los comandos ejecutados
    salida(" ")
    salida("*******  ACTUALIZAR NETEJA  *******")
    salida(" ")
    salida("git remote update")
    salida("git diff origin/" + branch)

    fich_act=path_install+'/.neteja.act'
    comando_sinsudo("rm " + fich_act)


    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Buscando actualizaciones de la aplicación")
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():
            GLib.idle_add(update_progress, "git fetch origin")
            time.sleep(0.5)
            comando_sinsudo("cd " + path_install + "/Neteja && git fetch origin")
            GLib.idle_add(update_progress, "git diff origin/" + branch)
            time.sleep(0.5)
            comando_sinsudo("cd " + path_install + "/Neteja && git diff origin/" + branch + " >> " + fich_act + " 2>&1")

            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()

    p = open(fich_act,"r+")
    # Leemos la salida de git diff -> si no hay cambios la variable será vacía
    hay_act = p.read()
    print(hay_act)
    p.close()
    if hay_act == "" :
        salida(" ")
        salida("NO HAY ACTUALIZACIONES DE NETEJA")
        # Si no hay actualizaciones devuelve el valor False
        return False
    else:
        salida(" ")
        salida("HAY ACTUALIZACIONES DE NETEJA")
        # Si no hay actualizaciones devuelve el valor False
        return True

# Actualización de Neteja en path_install y copia del .desktop en $HOME/.local/share/applications
#  Parámetros: 
#       password: contraseña introducida de superusuario
#       path_install: ruta de la instalación del script
def neteja_update(password,path_install):
   # Imprimimos en el fichero LOG los comandos ejecutados
    salida(" ")
    salida("Se va a actualizar")
    salida(" ")
    salida("cd "+ path_install + "/Neteja && git pull --force")
    salida("Creando neteja.desktop")

    def ejecucion_procesos():
        window = ProgressBar("NETEJA - Buscando actualizaciones de la aplicación")
        window.connect("destroy", Gtk.main_quit)

        def update_progress(txt):
            window.progressbar.pulse()
            window.progressbar.set_text(txt)
            return False

        def close_progress():
            window.destroy()

        def proceso():
            GLib.idle_add(update_progress, "git pull --force")
            time.sleep(0.5)
            comando_sinsudo("cd "+ path_install + "/Neteja && git pull --force")
            GLib.idle_add(update_progress, "Creando neteja.desktop")
            time.sleep(0.5)
            crear_desktop(path_install)
            GLib.idle_add(close_progress)

        window.show_all()

        ejecutar_thread = threading.Thread(target=proceso)
        ejecutar_thread.daemon = True
        ejecutar_thread.start()

    ejecucion_procesos()
    Gtk.main()



