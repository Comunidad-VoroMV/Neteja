"""
    MÓDULOS AUXILIARES USADAS POR MAIN Y EXE
"""


import gi
import os
import time
import threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

# Fichero de salida
home = os.environ['HOME']
fich_sal=home+'/neteja.log'


# Ejecuta un comando del sistema como si estubiera en el terminal con sudo. No muestra mensaje en pantalla 
#   y manda la salida al fichero de la variable definida fich_sal
#  Parámetros: 
#     cmd: comando a ejecutar
def comando(password,cmd):
    error = os.system('echo %s|sudo -S %s >> ' % (password, cmd) +fich_sal+' 2>&1')
    return error

# Ejecuta un comando del sistema como si estubiera en el terminal sin sudo. No muestra mensaje en pantalla 
#   y manda la salida al fichero de la variable definida fich_sal
#  Parámetros: 
#     cmd: comando a ejecutar
def comando_sinsudo(cmd):
    error = os.system(cmd)
    return error

# Comprueba si la contraseña de superusuario es correcta creando un fichero vacío en HOME llamado test_neteja.kk
#   y manda la salida en la variable definida fich_sal 
#  Devuelve el error del comando del sistema (si es 0 entnoces no ha habido problema,)
#  Parámetros: 
#     password: contraseña de superusuario
def comprobar_pass(password):
    cmd = 'dd if=/dev/null of=$HOME/test_neteja.kk bs=500M count=1  >> $HOME/neteja.log 2>&1'
    error = os.system('echo %s|sudo -S %s' % (password, cmd))
    return error

# Imprime en el fichero de la variable definida fich_sal 
#  Parámetros: 
#     txt: texto que se guardará en el fichero
def salida(txt):
	home = os.environ['HOME']
	with open(fich_sal,'a+') as writer:
		writer.write(txt + "\n")

	writer.close()

# Inicia el fichero LOG en la variable definida fich_sal 
#  Borra el fichero e imprime una cabecera con fecha y hora de ejecución
def ini_salida():
	home = os.environ['HOME']
	date_local = time.strftime("%A, %d %b %Y, %H:%M:%S", time.localtime())
	# Abre el fichero como escritura - se borra el contenido si exite
	writer = open(fich_sal,"w+")
	writer.close()
	salida("********************************************")
	salida("********************************************")
	salida("            Fichero LOG de NETEJA           ") 
	salida("********************************************")
	salida("********************************************")
	salida(" ")
	salida(" ")
	salida("Este fichero se borrará en la próxima ejecución de Neteja")
	salida("Si lo quieres mantener, cópialo en otro lugar o renómbralo")
	salida("Está grabado en tu Home: " + home + ". Su nombre es neteja.log")
	salida(" ")
	salida("Fecha y hora: " + date_local)
	salida(" ")
	salida(" ")


# Clase para objeto ProgressBar: barra de progreso, tipo pulso
# Parámetros:
#		Gtk.Windows: objeto ventana Gtk
#		titulo: título de la ventana
class ProgressBar(Gtk.Window):
	def __init__(self,titulo):
		super().__init__(title=titulo)
		self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)
		self.set_decorated(True)
		self.set_icon_from_file("img/logo_resized.png")
		self.set_border_width(10)
		self.set_default_size(500, 50)

		vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=16)
		self.add(vbox)

		self.progressbar = Gtk.ProgressBar()
		vbox.pack_start(self.progressbar, True, True, 0)

		self.progressbar.set_show_text(True)
		self.progressbar.set_ellipsize(2)

		self.progressbar.pulse()
		self.progressbar.set_pulse_step(0.2)
		self.timeout_id = GLib.timeout_add(200, self.update_progressbar)

	def update_progressbar(self):
		self.progressbar.pulse()
		return True
